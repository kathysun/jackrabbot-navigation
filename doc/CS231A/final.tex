\documentclass[11pt]{article}
\usepackage{fullpage,enumitem,amsmath,amssymb,graphicx}

\usepackage{lipsum}
\usepackage{multicol}
\usepackage{cite}

\usepackage{hyperref}
\usepackage{subcaption}

\begin{document}

\title{Learning to Walk: A Tale of Jackrabbot's Path-finding Adventures \\
\Large CS231A Winter 2015 Final Project}

\date{\today}

\author{
\begin{tabular}{c c c}
Kathy Sun && John Doherty \\
kathysun && doherty1 \\
\end{tabular} }

\maketitle

\begin{abstract}
The Jackrabbot is an autonomous delivery robot designed to share pedestrian walkways. In contrast to autonomous vehicles, this proposes the challenge of interacting safely with humans and bikers in a socially acceptable fashion. The goal of our project is to develop a path-planning algorithm for the Jackrabbot that learns over time how to navigate in the most natural way using both computer vision techniques and convolutional neural networks. Our best network currently achieves an accuracy of 65\% compared to the human-controlled path it trains on. Using qualitative measures, we believe an incredibly high accuracy may not be necessary to achieve the desired behavior, and that with a little more improvements the Jackrabbot may be ready for autonomy. This project will combine aspects of CS231A and CS231N with the Jackrabbot Research Project from Silvio Savarese's lab. The learning based off optical flow and HOG features will be used for CS231A, and the convolutional portion will be used for the CS231N course project.
\end{abstract}

\section{Introduction}

The Jackrabbot is an autonomous delivery robot designed to share pedestrian walkways. Path-finding and obstacle avoidance has been an active area in research since the development of autonomous vehicles. Whereas autonomous vehicles meant for the road follow clearly defined lanes, the Jackrabbot faces the additional challenge of interacting with humans and bikers in a socially acceptable fashion. For instance, it cannot interrupt pedestrian traffic or scare fellow travelers. The path of the Jackrabbot becomes more difficult than just simplifying its behavior to a set of rules, since what is socially acceptable is hard to define.  The goal of our project is to develop a path-planning algorithm for the Jackrabbot that learns from observing the path-finding behaviors of the humans that control it. We frame the problem as a classification problem in which the classes represent discretized orientation vectors the robot should take at each timestep based on the current image, or past images it has seen.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.5\linewidth]{pics/jack.png}
   \caption{The Jackrabbot. Link to video: \url{￼￼https://www.youtube.com/watch?v=e0sNH9ZUKK0}.}
\end{center}
\label{fig:long}
\label{fig:onecol}
\end{figure}

\section{Background}
\subsection{Related Work}

Research on this topic has been done since the late 1900's. ALVINN, an autonomous land vehicle in a neural network, developed at Carnegie Mellon attempted to solve similar challenges but in a driving environment \cite{alvinn}. ALVINN was a truck with a camera and laser range finder as sensory inputs. The automation behind ALVINN relied on a 3-layer neural network based on video and range information to learn the curvature of the road and keep ALVINN in the center of it. Instead of operating on the RGB video, ALVINN did its computation on the blue channel only and range information. The output layer classified the inputs into 45 turn curvatures and a road intensity feedback to recognize the road. ALVINN was able to accurately drive at a speed of 0.5 m/s along a 400 meter path through the CMU campus.

\subsection{Method}

Our work is somewhat similar to that done on ALVINN, an autonomous land vehicle in a neural network, developed at Carnegie Mellon \cite{alvinn}. ALVINN attempted to solve similar challenges but in a driving environment. We wish to apply similar techniques to the Jackrabbot, operating in a pedestrian environment. While the Jackrabbot has additional sensors for range detection, such as Lidar, we wish to only operate on the video camera input for the scope of this project.

We formulated our problem in the same manner as ALVINN, using neural networks to navigate the Jackrabbot. Like ALVINN, our output is also a vector of orientations or directions to turn. However, since ALVINN was a vehicle driving on clearly defined roads, a road intensity feedback was needed to detect the road. We did not include this feedback in our problem, as the Jackrabbot is meant to operate in a variety of settings where the road may not be clearly defined. 

Since computing power has greatly increased since the time ALVINN was developed, neural networks can be expanded to much deeper networks as convolutional neural networks. A lot more preprocessing is also feasible now. We wish to combine computer vision techniques to preprocess the video inputs before training on a neural network as well as trying different convolutional neural network architectures to teach the Jackrabbot where to turn. We reference material from CS231A \cite{A1, A2, A3, A4} and CS231N \cite{andrej}.

\section{Technical Approach}
\subsection{Summary}

We ultimately hope to train the Jackrabbot to navigate through a diverse set of pedestrian environments including sidewalks and hallways. The goal of this project, however, was to explore the feasibility of learning to navigate using a single camera from training examples provided by a human driver. To achieve this, we decided to constrain the task by focusing on hallway navigation. Specifically we wanted the Jackrabbot to learn to go straight down hallways without hitting stationary obstacles or pedestrians. Framing the problem in this way gives us a clear objective and reduces the number of possible situations we have to learn. Additionally, in the training examples we kept the robot at a constant speed, so the task was simplified to learning the orientation of the robot. The problem of predicting orientation was again simplified by turning it into a classification problem in which the robot chooses between going left, right, and straight at each timestep.

To solve this problem, we propose two main methods of visual processing as inputs into a linear Softmax classifier that will determine the direction the Jackrabbot should turn at any given moment. We will also train a linear classifier and a simple neural network on features obtained from computer vision techniques, such as histogram of gradients (HOG) features and optical flow, as a baseline for the convolutional neural network based learning. After characterizing the performance of these two main methods, shallow vs. deep learning, variants of each method based on modifying the inputs and features will be explored to further optimize accuracy. 

%We will collect our own dataset on the Jackrabbot by driving it around and recording its left and right front cameras and the associated joystick data. Using this dataset, we will develop, train, and test a convolutional neural network to predict the desired orientation given a sequence of images from multiple cameras. As will be discussed in the next section, we will be using a slightly modified architecture to take these multiple images as input. We will try a different architectures and operate on different subsets of this data. For example, we can compare the effectiveness of using images from varying sequence lengths (i.e. varying the value of k). How we will train these different models will be discussed more in the next section.

%Finally, once we have trained and tested these models we need a good way to compare their performance. One measure will simply be the classification accuracy (the percent of the test samples for which we correctly predict the direction). The problem with this metric is that it does not account for the correlation between the output classes. For example while two orientations may fall in different bins, they may actually be rather close to each other. We compute an evaluation metric on the average distance in bins between the correct direction and predicted direction. The final numeric metric that we will evaluate is the runtime of the network. For robotic applications, it is critical for the navigation system to work in real time.

%While numeric metrics are important, it is equally critical to develop a set of qualitative metrics to debug mistakes being made by the classifier. To visualize these errors, we will create maps that compare the ground truth trajectory to the predicted trajectory. 


\subsection{Implementation Details}

In this section we will describe the approach we used to build a dataset and train convolutional neural networks to learn Jackrabbot navigation from image inputs. As will be described, there are a number of unique challenges that arise when you try to build and use your own dataset to train deep neural networks.

\subsubsection{Dataset}
For this project we recorded our own dataset by driving the Jackrabbot through the hallways of the Gates Computer Science building. We simulated and encountered various scenarios while driving through Gates. We navigated around students walking in the hallways as well as various static obstacles including chairs, trashcans, and door frames. We recorded 122 sequences, each consisting of images from the front two stereo cameras and joystick data. Images were recorded at about 10 fps, and we ended up with about 26,000 images from each camera. To build the dataset, we associated each image with nearest joystick data point in time. We then classify this joystick data point as either left, right, or straight.

We ended up with a set of 26,000 images associated with 1 of 3 classes. But because of the nature of driving down a hallway, our class distribution was heavily skewed as seen in Table \ref{tab:dist}. We resolved this by eliminating some of our data to make the distribution more uniform. This left us with about 13,000 images. As we will discuss later, we experimented with learning on both the uniform and non-uniform class distributions.

For training and tuning our various networks, we used 85\% of our data for the training set and 15\% of our data for the validation set. We saved four recorded sequences not in either set for testing the accuracy.

\subsubsection{Feature Extraction}
Single images are often ambiguous, since it is difficult to tell how fast a person is walking from a single image. Thus, we had to incorporate some notion of time in our model. This has been done in other works by using optical flow images or replacing the channels of the input with past frames in the sequence \cite{A3}. We also wanted to explore the effect of HOG features in helping the neural network discover obstacles and valid pathways compared to using a convolutional neural network for computer vision. 

The dense optical flow was calculated using Gunner Farneback’s algorithm based on polynomial expansion\cite{optflow}. The optical flow was computed between the current frame and the previous frame, giving a 2D vector field representing the motion of the pixels. This was stacked on top of the greyscale image of the current frame to give a 3 channel image to put through the neural networks, shown in Figure \ref{fig:optflow}.

We stacked sequences of 5 greyscale frames to generate a 5-channel input into the neural networks as depicted in Figure \ref{fig:stacked}. We also played with the interval between the preceding frames to generate a 5-channel input starting 10 frames before the current frame sampled at every other frame.

The histogram of gradients was calculated for each frame by computing the gradient along the x and y axes, computing the histograms, and normalizing across blocks. The result, visualized in Figure \ref{fig:hog}, was then flattened into a feature vector and stacked with the four frames before it to add temporal information. The HOG features were computed using 6x6 pixels per cell wit 3x3 cells per block with 9 orientation grids, since these were determined optimal for human detection by Dalal and Triggs \cite{hog}.

\begin{figure}[t]
\centering
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=\linewidth]{pics/frame0155.jpg}
	\caption{Optical Flow + Greyscale Image}
	\label{fig:optflow}
\end{subfigure}
\hfill
\begin{subfigure}{0.3\linewidth}
	\centering	
	\includegraphics[width=\linewidth]{pics/stacked.png}
	\caption{Stacked Greyscale Images}
	\label{fig:stacked}
\end{subfigure}
\hfill
\begin{subfigure}{0.35\linewidth}
	\centering	
	\includegraphics[width=\linewidth]{../../visualization/hog.png}
	\caption{HOG Features Images}
	\label{fig:hog}
\end{subfigure}
   \caption{Inputs}
\label{fig:inputs}
\label{fig:onecol}
\end{figure}


\subsubsection{Networks}
We experimented with a combination of the inputs described above and the network architectures listed below. They include a linear classifier for baseline comparison and neural networks and convolutional neural networks. We observed that the models overfit our data so we added dropout layers for regularization. All networks follow a inverse decay learning rate policy.

\begin{itemize}
\item Linear Softmax Classifier: 
\emph{FC-3 - Softmax}
\item 2-layer Neural Network with 500 hidden neurons: 
\emph{FC-500 - ReLU - Drop-1/2 - FC-3 - Softmax}
\item 2-layer Convolutional Neural Network: 
\emph{Conv5-10 - ReLU - Pool2 - Conv5-30 - ReLU - Pool2 - FC-500 - ReLU - Drop-1/2 - FC-3 - Softmax}
\item 3-layer Convolutional Neural Network: 
\emph{Conv5-10 - ReLU - Pool2 - Conv5-30 - ReLU - Pool2 - Conv5-30 - ReLU - Pool2 - FC-500 - ReLU - Drop-1/2 - FC-500 - ReLU - Drop-1/2 - FC-3 - Softmax}
\end{itemize}

\subsubsection{Code}
To collect data, we used a VirtualBox with Ubuntu to connect to the JackRabbot. We saved data in ROS in bagfile format. From the bagfile format, we exported the left and right greyscale frames as well as the joystick data and saved them as jpeg images and a text file of labels using a Python script. The frame rate of the images (10 fps) is much slower than that of the joystick data, so we sampled the joystick point right before the image was captured.

After the data was exported as jpegs, preprocessing, i.e. data augmentation, normalization, and feature extraction, was done in Python. The optical flow was extracted from the images along with a new label file using OpenCV \cite{A5} in Python. The HOG features were computed and displayed using Python's Scikit-Image library. We also doubled our data by mirroring the images and flipping the labels, however this did not perform well, so we used the original dataset.

The data was split into a training, validation and test set and converted into lmdb format using C++. The mean of the images in the training set was also saved in this step.

Different nets were created and ran on different preprocessed lmdb formatted data using Caffe \cite{caffe}. We used scripts to sweep through the learning rate hyperparameter and plot them.

All the visualizations and testing of our models were done in Python.

All the code can be found in our git repository at: \\ \url{https://bitbucket.org/kathysun/jackrabbot-navigation}

\section{Experiment}
\subsection{Sanity Checks}

\begin{figure*}[ht]
\begin{minipage}{0.4\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../../data_lmdb/plots/histLR-RStick.png}
   \caption{Plot of Raw Joystick Data. The top plot shows the histogram of raw joystick data with negative being in the right direction and positive, the left. The bottom plot shows the joystick values over time of all the data sequences. }
	\label{fig:joy}
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../../preprocessing/plots/optvslabel.png}
   	\caption{Plot of Labels vs. Average Optical Flow of Image to determine if labels are lined up correctly with each image. }
	\label{fig:optvslab}
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../../learning/5_len_all_data_2_layer/test_videos_2015-03-15-18/video03/confusion.png}
   	\caption{Confusion Matrix of the Tested Results to visualize which class the classifier has the most trouble with.}
   \label{fig:conf}
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../../learning/5_len_all_data_2_layer/test_videos_2015-03-15-18/video03/labels.png}
   	\caption{Plot of Predicted Labels vs. Ground Truth Labels Over Time.}
\label{fig:labelsvtime}
\end{minipage}
\end{figure*}

\begin{table}[ht]
\begin{center}
\begin{tabular}{|l||c|c|c|}
\hline
& Left & Straight & Right \\
\hline\hline
Before & 15\% & 71\% & 14\% \\
After & 33.3\% & 33.6\% & 33.1\% \\
\hline
\end{tabular}
\end{center}
	\caption{Class Distribution Before and After Normalization}
\label{tab:dist}
\end{table}

In addition to visualizing our outputs, we implemented a few visualizations for our inputs as sanity checks. 

To get a feel for the direction input labels, we plotted the distribution of the raw joystick data shown in Figure \ref{fig:joy}. As you can see the data is evenly centered around zero with approximately the same number of left and right turns. However, the Jackrabbot is moving straight a significant amount of time. Since there are also two peaks at -1 and 1 (the edges of the joystick), but the rest of the data remains relatively close to 0, it was difficult to determine where the bin edges should be when considering more than 3 bins, which is why we decided on 3 bins separated by the sign.

Since most of the data collected is of the Jackrabbot going straight, over 70\% of the joystick data is at 0. As a baseline for our model, we'd like it to perform better than a naive model that just went straight no matter what, meaning it'd have to achieve a validation accuracy $>$ 71\%. We redistributed our dataset by removing frames in the straight class to normalize the class distribution to be uniform before training. Table \ref{tab:dist} shows the class distribution before and after redistribution. Before normalizing the classes, the point at which the loss would start at would vary wildly between training sessions. After creating a uniform class distribution, the loss started at $\approx$ -ln(1/3) = 1.0986 and the training and validation accuracies started at $\approx$ 33\%.

To ensure our labels were properly lined up with our images, we plotted the average optical flow of the Jackrabbot's camera feed at each timestep against the labels we assigned to it. The reasoning being that the average optical flow will reflect the direction the Jackrabbot has turned, which should be a reaction to the control mechanism, the joystick. As you can see from Figure \ref{fig:optvslab}, the joystick control precedes the flow as expected. To our surprise, however the latency is quite noticeable, lasting over multiple frames, explaining why visualizing via a labeled video was not obvious.

From the confusion matrix in Figure \ref{fig:conf}, the best network rarely seems to confuse left from right (1 for 2 or 2 for 1), which is a good sign. Most of the mis-classifications happen between turn a direction and going straight, which could be due to some noise in the correct labels or some time offset in the predictions.

\subsection{Results}
\begin{figure*}[t]
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=\linewidth]{../../learning/1_len_opt_neural/plots/accuracy_2015-03-18-16-00-52.png}
	\includegraphics[width=\linewidth]{../../learning/1_len_opt_neural/plots/loss_2015-03-18-16-00-52.png}
	\caption{Single Optical Flow + Greyscale - 2-Layer Net}
	\label{fig:resA}
\end{subfigure}
\hfill
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=\linewidth]{../../learning/5_len_hog_neural/plots/accuracy_2015-03-18-29.png}
	\includegraphics[width=\linewidth]{../../learning/5_len_hog_neural/plots/loss_2015-03-18-29.png}
	\caption{Stacked HOG Features - 2-Layer Net }
	\label{fig:resB}
\end{subfigure}
\hfill
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=\linewidth]{../../learning/5_len_all_data_2_layer/plots/accuracy_2015-03-15-35.png}
	\includegraphics[width=\linewidth]{../../learning/5_len_all_data_2_layer/plots/loss_2015-03-15-35.png}
	\caption{Stacked Greyscale - 2-Layer ConvNet }
	\label{fig:resC}
\end{subfigure}
\caption{Training Results After Normalization of Class Distribution}
\label{fig:res}
\end{figure*}

\begin{table}
\begin{center}
\begin{tabular}{|c|c|c||c|}
\hline
Input Data & Depth & Net & Test Accuracy \\
\hline\hline
Single Optical Flow + Greyscale & 3 & Linear Classifier& 53.97\% \\
Single Optical Flow + Greyscale & 3 & 2-Layer Neural & 58.06\% \\
Stacked Optical Flow + Greyscale & 15 & Linear Classifier & 51.86\% \\
Stacked HOG Features & 5 & Linear Classifier& 56.41\% \\
Stacked HOG Features & 5 & 2-Layer Neural & 57.87\% \\
Single Greyscale & 1 & 3-Layer ConvNet & 59.95\% \\
Stacked Greyscale & 5 & 2-Layer ConvNet & 65.12\%  \\
Stacked Greyscale Every Other Frame & 5 & 2-Layer ConvNet & 62.47\% \\
Stacked Greyscale Every Other Frame & 5 & 3-Layer ConvNet & 54.14\% \\
\hline
\end{tabular}
\end{center}
	\caption{Results after Normalizing Class Distribution}
\label{tab:res}
\end{table}

For comparison, we used a linear classifier on optical flow images and HOG features as an additional baseline to the always-go-straight model (before normalization: 70\%, after: 33\% accuracy). The linear classifier had trouble generalizing from the training set to the validation set and performed well worse than the always-go-straight model. Before normalizing the dataset, the validation accuracy was much lower or equal to the always-go-straight model. After normalization, the validation accuracy, although numerically lower, was 30\% higher than the always-go-straight or random guessing model.

Based on Table \ref{tab:res}, the 2-Layer Networks performed the best for all types of inputs, with the Convolutional Neural Network at the highest test accuracy of 65.12\%. The extra information encoded in the optical flow did not prove as useful as stacked inputs, since the HOG Features also performed relatively well when stacked into a sequence of 5. 

When compared to the stacked HOG features, the single optical flow input performed better based on the testing accuracy. However, looking at the graph of the validation accuracies in Figure \ref{fig:res} seems to suggest that stacking 5 images offers more valuable information than just the optical flow alone since our test set was much smaller than our validation set.

\begin{figure}[t]
\begin{minipage}{0.48\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{pics/video.png}
   	\caption{Labeled Input Stream. The green dot represents a correct prediction and the yellow and red dots represent the ground truth and the predicted label respectively in a mislabeled instance. The position of the dots on the input images represent the direction of the joystick data. A dot on the left edge, center, and right edge signify a left turn, straight, and right turn respectively. Link to video: \url{https://www.youtube.com/watch?v=54lI4STjJQk}.}
	\label{fig:vid}
\end{minipage}
\hfill
\begin{minipage}{0.48\linewidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../../learning/5_len_all_data_2_layer/test_videos_2015-03-15-18/video03/traj.png}
	\caption{Top View of the Predicted and Ground Truth Labels.}
	\label{fig:traj}
\end{minipage}
\end{figure}

To qualitatively view the performance of our model, we labeled videos of our test sequences with the predicted and correct labels, Figure \ref{fig:vid}. Even though some images were mislabeled, they still made sense and some were just off by a frame or two. This could still be acceptable behavior when deployed to the Jackrabbot in real life.

To better understand when there is the discrepancy between the predicted and ground truth labels, we plotted the value of the predicted and ground truth labels over time in Figure \ref{fig:labelsvtime}. Predicted labels seem to be well correlated with the correct ground truth labels with some slightly offset-ted. As you can see, the predicted labels jump around less than the correct labels, indicating this may be desirable behavior that would be low in accuracy.

We also wanted to visualize the physical distance between the predicted and correct trajectories Jackrabbot would take, so we plotted the top view in Figure \ref{fig:traj}. These trajectories are approximate since our labels are separated into 3 classes. The Jackrabbot was moving at a constant velocity of 0.5 m/s forward in our dataset and the maximum angular velocity was set at 1.571 rad/s. Each label was taken at 10 frames/s. From this, the average angle of rotation, $1.8^o$ /frame, was found from the average joystick positive and negative values, $\pm0.2$, and used to plot the vector at each timestep. 


\section{Conclusion}

The results from the 2-Layer convolutional neural net with 5 consecutive greyscale frames stacked as input achieved the highest accuracy of 65.12\%. This is 32\% better than random guessing. Despite having an accuracy of only 65.12\%, from the video stream, we think we are close to being able to deploy and test on the Jackrabbot itself. Even though, we called our human-controlled joystick data as "ground truth", there are many paths Jackrabbot could take that would be nearly identical to "ground truth" and produce the desired behavior. Futhermore, our human-controlled driving is not optimal and varies from driver to driver, so our metric of matching the ground truth labels exactly is indicative but not precise in measuring the optimal behavior.

A single optical flow and greyscale image offered approximately the same information as a sequence of 5 HOG feature vectors, with the latter being slightly better. This suggests that temporal information is critical to solving our problem. The HOG features, however, provided less information than the convolutional layers which could encode these gradients in its filers. Stacking the frames and normalizing the class distributions were the most effective, each improving our accuracy by over 10\%. We would like to try running the optical flow input through the convolutional network as well, but ran into out of memory issues, since it has triple the depth. This would be something to work on in the future. 

We learned that good visualizations are key to making valuable and sensible adjustments to the network. Without them, results can be very misleading and efforts to tweak to hyperparameters fruitless. Quantitative measures, such as accuracy, can also be misleading since skewed datasets to a specific class will give high accuracy for models that do nothing, such as the always-go-straight model. That is why having baselines to reference and having qualitative measures to gauge whether the predictions make sense is really important.

\subsection{Future Work and Improvements}
Our next steps include deploying this model onto the Jackrabbot. But before that, we would like to further improve our model by means of more preprocessing of the label data and integrating depth as well as trying Recurrent Neural Networks \cite{RNN}.

Most the joystick data is close to zero (Figure \ref{fig:joy}) since our turns are mostly small turns, which depending on the driver or the sense of urgency, was controlled via small adjustments to the joystick or quick bursts to the edges of the joystick axis. This variability in driver-to-driver data is also apparent in the bottom plot where the second half of the sequences have a smaller amplitude than the first half. We also do not know the exact mapping of joystick control data to the angular velocity of the Jackrabbot. For this reason, we think it would be a good idea to smooth out the joystick data either with a simple moving average or a Kalman filter or use accelerometer data as the labels. This way, the labels would be easier to map and bin to several ($>$ 3) direction vectors. They also wouldn't jump around so much over time, and two very similar images won't produce vastly different labels, making it easier to learn. 

We originally planned to use the stereo cameras to calculate the depth map and integrate that into our network. We were able to create the depth map, but the left and right images were captured out of sync, resulting in erroneous depths. The Jackrabbot has a Lidar sensor on it and is capable of capturing the depth information itself, so we can integrate that in the next iteration. 

As for extending this to more complex environments, there is a lot of work that needs to be done. First of all, we need to encode some sense of destination or goal. In our experiments that was given to us because we were trying to go straight and had only one way to get there. In any complex environment, there will be a number of ways to avoid obstacles and reach the destination. This and other challenges will need to be solved for a fully automated system, but we believe this proof of concept is a good first step.

\begin{thebibliography}{1}

\bibitem{alvinn}
Pomerleau, Dean A., "ALVINN, an autonomous land vehicle in a neural network", Carnegie Mellon University, 1989. 

\bibitem{optflow}
Farnebäck, Gunnar. "Two-frame motion estimation based on polynomial expansion." In Proceedings of the 13th Scandinavian conference on Image analysis (SCIA'03), Josef Bigun and Tomas Gustavsson (Eds.). Springer-Verlag, Berlin, Heidelberg, 363-370. 2003.

\bibitem{hog}
Dalal, N.; Triggs, B., "Histograms of oriented gradients for human detection," Computer Vision and Pattern Recognition, 2005. CVPR 2005. IEEE Computer Society Conference on , vol.1, no., pp.886,893 vol. 1, 25-25, June 2005.

\bibitem{RNN}
Donahue, Jeff and Hendricks, Lisa and Guadarrama, Sergio and Rohrbach, Marcus and Venugopalan, Subhashini and Saenko, Kate and Darrell, Trevor. \emph{Long-term Recurrent Convolutional Networks for Visual Recognition and Description}. CoRR, vol. abs/1411.4389, 2014. 

\bibitem{caffe}
Jia, Yangqing and Shelhamer, Evan and Donahue, Jeff and Karayev, Sergey and Long, Jonathan and Girshick, Ross and Guadarrama, Sergio and Darrell, Trevor. \emph{Caffe: Convolutional Architecture for Fast Feature Embedding}. arXiv preprint arXiv:1408.5093, 2014.

% CS231N Notes
\bibitem{andrej}
Li, Fei-Fei and Karpathy, Andrej. "CS231n: Convolutional Neural Networks for Visual Recognition", 2015. http://cs231n.github.io.

% CS231A Textbooks
\bibitem{A1}
D. A. Forsyth and J. Ponce. \emph{Computer Vision: A Modern Approach (2nd Edition)}. Prentice Hall, 2011.
\bibitem{A2}
R. Hartley and A. Zisserman. \emph{Multiple View Geometry in Computer Vision}. Cambridge University Press, 2003. 
\bibitem{A3}
R. Szeliski. \emph{Computer Vision: Algorithms and Applications}. Springer, 2011. 
\bibitem{A4}
D. Hoiem and S. Savarese. "Representations and Techniques for 3D Object Recognition and Scene Interpretation", \emph{Synthesis lecture on Artificial Intelligence and Machine Learning}. Morgan Claypool Publishers, 2011. 
\bibitem{A5}
Gary Bradski, Adrian Kaehler. \emph{Learning OpenCV}, O'Reilly Media, 2008. 

\end{thebibliography}

\end{document}