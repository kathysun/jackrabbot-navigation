\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
\setcounter{page}{1}
\begin{document}

%%%%%%%%% TITLE
\title{Learning to Walk: A Tale of Jackrabbot’s Path-finding Adventures \\ CS231N Winter 2015 Project Milestone}

\author{John Doherty\\
Stanford University\\
Stanford, California 94305\\
{\tt\small doherty1@stanford.edu}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Kathy Sun\\
Stanford University\\
Stanford, California 94305\\
{\tt\small kathysun@stanford.edu}
}

\date{\today}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
The Jackrabbot is an autonomous delivery robot designed to share pedestrian walkways. In contrast to autonomous vehicles, this proposes the challenges of interacting safely with humans and bikers in a socially acceptable fashion. The path of the Jackrabbot then becomes nontrivial, as it cannot block pedestrian traffic or scare fellow travelers. The goal of our project is to develop a path-planning algorithm for the Jackrabbot that learns from observing the path-finding behaviors of the humans it will share walkways with, using both computer vision techniques and convolutional neural networks. This project will combine aspects of CS231A and CS231N with the Jackrabbot Research Project from Silvio Savarese's lab. We plan to use this for both class projects.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Background}

<<<<<<< HEAD
Path-finding and obstacle avoidance have been active areas of research since the development of autonomous vehicles. The Jackrabbot, an autonomous delivery robot, faces the additional challenge of interacting safely with humans and bikers on pedestrian walkways in a socially acceptable fashion. We want the Jackrabbot to learn path-finding behaviors by being driven by humans, given only what it observes from a single camera. 

We ultimately hope to train the Jackrabbot to navigate through a diverse set of pedestrian environments including sidewalks and hallways. The goal of this project, however, was to explore the feasibility of learning to navigate using a single camera from training examples provided by a human driver. To achieve this, we decided to constrain the task by focusing on hallway navigation. Specifically we wanted the Jackrabbot to learn to go straight down hallways without hitting stationary obstacles or pedestrians. Framing the problem in this way gives us a clear objective and reduces the number of possible situations we have to learn. Additionally, in the training examples we kept the robot at a constant speed, so the task was simplified to learning the orientation of the robot. The problem of predicting orientation was again simplified by turning it into a classification problem in which the robot chooses between going left, right, and straight at each timestep.

Our work is somewhat similar to that done on ALVINN, an autonomous land vehicle in a neural network, developed at Carnegie Mellon \cite{alvinn}. ALVINN attempted to solve similar challenges but in a driving environment. We wish to apply similar techniques to the Jackrabbot, operating in a pedestrian environment.

We will reference material from CS231A \cite{A1, A2, A3, A4} and CS231N \cite{andrej}.
=======
Path-finding and obstacle avoidance has been an active area in research since the development of autonomous vehicles. The Jackrabbot, an autonomous delivery robot, faces the additional challenges of interacting safely with humans and bikers on pedestrian walkways in a socially acceptable fashion. We wish to allow the Jackrabbot to learn path-finding behaviors from being driven by humans, given what it is observing in its camera feed. We frame the problem as a classification problem in which the classes represent discretized orientation vectors the robot should take at each timestep based on the current image, or past images it has seen.

ALVINN, an autonomous land vehicle in a neural network, developed at Carnegie Mellon attempted to solve similar challenges but in a driving environment \cite{alvinn}. We wish to apply similar techniques of machine learning with respect to computer vision on a robot in a pedestrian environment and characterize the effectiveness of different algorithms.
Not only will we compare several different convolutional neural network structures, but we will also compare our algorithm to supervised learning on the same dataset but using other computer vision techniques such as object detection and tracking as features. We will reference material from CS231A \cite{A1, A2, A3, A4} and CS231N \cite{andrej}.
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.8\linewidth]{pics/jack.png}
\end{center}
   \caption{The Jackrabbot. Link to video \href{￼￼https://www.youtube.com/watch?v=e0sNH9ZUKK0}{here}.}
\label{fig:long}
\label{fig:onecol}
\end{figure}

<<<<<<< HEAD
=======
%-------------------------------------------------------------------------
\section{Problem Statement}

In order to train the Jackrabbot to navigate like a human, we will manually drive the robot through crowded environments. While driving the robot, we will record the images being captured by all of the cameras $(c_1...c_n)$ on the device as well as the manual control input to the robot. The manual control input at time t, $\theta_t$ will be recorded as the difference between the orientation at time t, $\phi_t$ and at time t-1, $\phi_{t-1}$. To make this a classification problem, $\theta_t$ will represent a bin of angles. For example, if $\phi_t$ and $\phi_{t-1}$ are measured in degrees in [-180...180], we compute $\theta$ as: 
\begin{center} 
$\theta = \lfloor((\phi_t - \phi_{t-1}) / binsize)\rfloor$
\end{center}

After driving the robot for some time, T, we will have recorded $c_1,t...c_n,t, \theta_t$ for every t in 1..T. After recording sufficient data, we will build up a dataset of inputs and outputs that the robot can use to learn to replicate and generalize the navigation patterns recorded from the human driver. Each input will be a time sequenced set of images from all of the cameras $(c_{1,t}...c_{n,t}, c_{1,t-1}...c_{n,t-1}...c_{1,t-k}...c_{n,t-k})$, and the output will be $\theta_t$.

Using this dataset, we will develop, train, and test a convolutional neural network to predict the desired orientation given a sequence of images from multiple cameras. As will be discussed in the next section, we will be using a slightly modified architecture to take these multiple images as input. We will try a different architectures and operate on different subsets of this data. For example, we can compare the effectiveness of using a single camera vs. multiple cameras or using images from varying sequence lengths (i.e. varying the value of k). How we will train these different models will be discussed more in the next section.

Finally, once we have trained and tested these models we need a good way to compare their performance. One measure will simply be the classification accuracy (the percent of the test samples for which we correctly predict $\theta$). The problem with this metric is that it does not account for the correlation between the output classes. For example while two orientations may fall in different bins, they may actually be rather close to each other. We compute an evaluation metric on the average distance in bins between the correct $\theta$ and predicted $\theta$. The final numeric metric that we will evaluate is the runtime of the network. For robotic applications, it is critical for the navigation system to work in real time.

While numeric metrics are important, it is equally critical to develop a set of qualitative metrics to debug mistakes being made by the classifier. To visualize these errors, we will create maps that compare the ground truth trajectory to the predicted trajectory. We will also visualize the weights for the first convolutional layer of the network. We can decompose the weights for our multi-image input to visualize filters that act over x, y, and t.

To evaluate the performance between different methods, we would also like to take benchmarks of the total training and test time of each method and plot them against the test accuracy.

>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099

%-------------------------------------------------------------------------
\section{Technical Approach}
In this section we will describe the approach we used to build a dataset and train convolutional neural networks to learn Jackrabbot navigation from image inputs. As will be described, there are a number of unique challenges that arise when you try to build and use your own dataset to train deep neural networks.

\subsection{Dataset}
For this project we recorded our own dataset by driving the JackRabbot through the hallways of the Gates Computer Science building. We recorded 122 sequences, each consisting of images from the front two stereo cameras and joystick data. Images were recorded at about 10 fps and we ended up with about 26,000 images from each camera. To build the dataset, we associated each image with nearest joystick datapoint in time. We then classify this joystick datapoint as either left, right, or straight.

We ended up with a set of 26,000 images associated with 1 of 3 classes. But because of the nature of driving down a hallway, our class distribution was heavily skewed as seen in table XXXXXXX. We resolved this by eliminating some of our data to make the distribution more uniform. This left us with about 13,000 images. As we will discuss later, we experimented with learning on both the uniform and non-uniform class distributions.

For actually training the various learning algorithms, we used 85\% of our data training and 15\% of our data for testing. We saved for recorded sequences for testing.

\subsection{Convolutional Neural Network}

We would like to implement and characterize all the following ideas, but in the interest of time and feasibility, we will start with the first two and pick which variations of the input to implement next based on the results we find. 

\begin{itemize}
\item Transfer learning with single image inputs: Take pre-trained ConvNet on ImageNet with last fully-connected layer removed and retrain with collected Jackrabbot dataset.
\item Custom architecture trained solely on the Jackrabbot dataset: Conv-Relu-Pool-Conv-Relu-Pool-FC with frames of 3 or more images as additional depth of the inputs to add the dimension of time/motion
\item Concatenate images of extraneous cameras oriented in different directions like a panorama as inputs.
\item Stack frames of stereo camera to account for physical depth in the inputs.
\item Use the depth map and motion map features obtained above as inputs.
\end{itemize}

%-------------------------------------------------------------------------
\section{Experiment}

\subsection{Implementation}
To collect data, we used a VirtualBox with Ubuntu to connect to the JackRabbot. We saved data in ROS in bagfile format. From the bagfile format, we exported the left and right greyscale frames as well as the joystick data and saved them as jpeg images and a text file of labels using a Python script. 

<<<<<<< HEAD
After the data was exported as jpegs, preprocessing was done in Python. The optical flow was extracted from the images and along with a new label file using OpenCV in Python.
=======
After the data was exported as jpegs, preprocessing, ie data augmentation, normalization, and feature extraction, was done in Python. The optical flow was extracted from the images along with a new label file using OpenCV \cite{A5} in Python. We also doubled our data by mirroring the images and flipping the labels, however this did not perform well, so we used the original dataset.
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099

The data was split into a training, validation and test set and converted into lmdb format using C++. The mean of the images in the training set was also saved in this step.

Different nets were created and ran on different preprocessed lmdb formatted data using Caffe. We used scripts to sweep through the learning rate hyperparameter and plot them.

All the visualizations and testing of our models were done in Python.

All the code can be found in our git repository at \url{https://kathysun@bitbucket.org/kathysun/jackrabbot-navigation.git}

\subsection{Sanity Checks}
baseline
plot class distributions

\begin{table}
\begin{center}
\begin{tabular}{|l|c|c||c|c|r|}
\hline
& Left & Straight & Right \\
\hline\hline
Before & 15\% & 71\% & 14\% \\
After & 33.3\% & 33.6\% & 33.1\% \\
\hline
\end{tabular}
\end{center}
\caption{Class Distribution before and after Redistribution}
\end{table}
Sanity check loss starts at -ln(1/3) = 1.0986
Accuracy starts at 33%

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../data_raw/plots/histLR-RStick.png}
\end{center}
   \caption{Plot of Raw Joystick Data to determine the distribution of joystick data and characterize the sensor data. The top plot shows the histogram of raw joystick data which ranges from [-1,1] with negative being in the right direction and positive, the left. The bottom plot shows the joystick values over time of all the data sequences. As you can see the data is pretty evenly centered around zero with approximately the same number of left and right turns. However, the Jackrabbot is moving straight a significant amount of time. Since there are also two peaks at -1 and 1 (the edges of the joystick), but the rest of the data remains relatively close to 0, it is difficult to determine where the bin edges should be when considering more than 3 bins. Most the data is close to zero since our turns are mostly small turns, which depending on the driver or the sense of urgency, is controlled via small adjustments to the joystick or quick bursts to the edges of the joystick axis. This variability in driver-to-driver data is also apparent in the bottom plot where the second half of the sequences has a smaller amplitude than the first half. We also do not know the exact mapping of joystick control data to the angular velocity of the Jackrabbot. For this reason, we think it'd be a good idea to smooth out the joystick data either with a simple moving average or a Kalman filter or use accelerometer data as the labels. This way, the labels would be easier to map and bin to direction vectors. They also wouldn't jump around so much over time, and two very similar images won't produce vastly different labels, making it easier to learn. }
\label{fig:long}
\label{fig:onecol}
\end{figure}

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../preprocessing/plots/optvslabel.png}
\end{center}
   \caption{Plot of Labels vs. Average Optical Flow of Image to determine if labels are lined up correctly with each image. As you can see the joystick control comes before the flow indicating the Jackrabbot has turned, as expected. The latency seems quite noticeable over multiple frames.}
\label{fig:long}
\label{fig:onecol}
\end{figure}



\begin{figure}[t]
\begin{center}
\fbox{\rule{0pt}{2in} \rule{0.9\linewidth}{0pt}}
%\includegraphics[width=0.9\linewidth]{}
\end{center}
   \caption{Confusion Matrix of the Tested Results to visualize which class the classifier has the most trouble with.}
\label{fig:long}
\label{fig:onecol}
\end{figure}

\section{Results}
\begin{figure*}
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=0.2\linewidth]{pics/simple-net.png}
	\caption{2-Layer Net}
	\label{fig:2-layer}
\end{subfigure}
\hfill
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=0.2\linewidth]{pics/2-layer.png}
	\caption{2-Layer Net}
	\label{fig:2-layer}
\end{subfigure}
\hfill
\begin{subfigure}{0.3\linewidth}
	\centering
	\includegraphics[width=0.2\linewidth]{pics/3-layer.png}
	\caption{3-Layer Net}
	\label{fig:3-layer}
\end{subfigure}

\caption{Convolutional Neural Net Architectures}
\end{figure*}

\subsection{Intermediate/Preliminary Results}
Since the Jackrabbot will not arrive until Tuesday, Feb 17, we have not been able to collect our dataset yet. Instead, to prepare for its arrival, we have set up OpenCV and Caffe and tested the library. We have also started implementing the data visualizations as well as converting the images into the proper format to stack multiple frames. Since we plan to pre-train our network on ImageNet, we have tested a couple networks and chosen GoogleNet to use for transfer learning.

Preliminary results from poster
optflow vs stacked

<<<<<<< HEAD

Data augmentation - mirroring

Input Data

\begin{table}
\begin{center}
\begin{tabular}{|l|c|c||c|c|r|}
\hline
Input Data & Depth & Net & Train Accuracy & Validation Accuracy & Test Accuracy \\
\hline\hline
2D Optical Flow + Greyscale & 3 & Linear Classifier - Softmax & 0.0\% & 0.0\% & 0.0\% \\
Single Greyscale & 1 & Conv - Conv - Softmax & 0.0\% & 0.0\% & 0.0\% \\
Stacked Greyscale & 5 & Conv - Conv - Softmax & 0.0\% & 0.0\% & 0.0\% \\
Stacked Greyscale Sample Every Other Frame& 5 & Conv - Conv - Softmax & 0.0\% & 0.0\% & 0.0\% \\
\hline
\end{tabular}
\end{center}
\caption{Intermediate Results}
\end{table}
=======
For comparison, we used a linear classifier on optical flow images as an additional baseline to the always-go-straight model (70\% accuracy). This baseline had trouble generalizing from the training set to the validation set, visible in Figure \ref{fig:p_resA}, and performed well worse than the always-go-straight model. 
When we put a single greyscale image into a simple two-layer convnet \ref{fig:p_resB}, the accuracy improved but was still less than always going straight method.
Once we introduced the time dimension into the convnet by stacking a sequence with the 5 frames preceding the label, the accuracy of our model increased passed our always-go-straight baseline to 80\%. From \ref{fig:p_resC}, the validation accuracy closely follows the training accuracy after a couple hundred iterations, suggesting that the capacity of our model is not large enough and that we should add more data. The learning rate also appears to be too high since the loss levels off and stops decreasing.
We also suspected that we needed more data. In an attempt to double our dataset, we augmented our dataset by mirroring the images and flipping the labels, but this resulted in a decrease in accuracy \ref{fig:p_resD}. As a result, we did not use the augmented data and instead collected more data. 
 
\begin{table*}
\begin{center}
\begin{tabular}{|c|c|c||c|}
\hline
Input Data & Depth & Net & Test Accuracy \\
\hline\hline
Optical Flow + Greyscale & 3 & Linear Softmax Classifier & 51.68\% \\
Single Greyscale & 1 & Simple Net & 70.96\%  \\
Stacked Greyscale & 5 & Simple Net & 81.02\% \\
Mirrored Stacked Greyscale & 5 & Simple Net & 67.20\% \\
\hline
\end{tabular}
\end{center}
\caption{Results Before Normalization of Class Distribution}
\end{table*}

\begin{figure*}
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/optflow_softmax/plots/accuracy_iter_5e-5.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/optflow_softmax/plots/lr_iter_5e-5.png}
	\caption{Optical Flow + Greyscale - Linear Softmax Classifier}
	\label{fig:p_resA}
\end{subfigure}
\hfill 
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/simple_net/plots/accuracy_sing_1e-4.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/simple_net/plots/loss_sing_1e-4.png}
	\caption{Single Greyscale - Simple Net }
	\label{fig:p_resB}
\end{subfigure}
\hfill
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/multi_image_net/plots/accuracy_2015-03-11-10.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/multi_image_net/plots/loss_2015-03-11-10.png}
	\caption{Stacked Greyscale - Simple Net}
	\label{fig:p_resC}
\end{subfigure}
\hfill
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/multi_image_net/plots/accuracy_2015-03-14-44.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/multi_image_net/plots/loss_2015-03-14-44.png}
	\caption{Mirrored Stacked Greyscale - Simple Net}
	\label{fig:p_resD}
\end{subfigure}
\caption{Results Before Normalization of Class Distribution}
\end{figure*}

\subsection{After Normalization of Class Distribution}
In addition to collecting more data, we normalized the class distributions to a uniform distribution to help the Jackrabbot better learn the left and right classes. This means our always-go-straight baseline now has 33\% accuracy. When we trained these on 3-layer convnets, the models seemed to heavily overfit the data \ref{fig:resA}, so we added dropout for regularization. The results from the best nets after normalizing the class probabilities are shown in Table \ref{tab:res}.
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099

\begin{table}
\begin{center}
<<<<<<< HEAD
\begin{tabular}{|l|c|c||c|c|r|}
\hline
Input Data & Depth & Net & Train Accuracy & Validation Accuracy & Test Accuracy \\
=======
\begin{tabular}{|c|c|c||c|}
\hline
Input Data & Depth & Net & Test Accuracy \\
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099
\hline\hline
Single Greyscale & 1 & 3-Layer Net & 59.95\% \\
Stacked Greyscale & 5 & 2-Layer Net & 65.12\%  \\
Stacked Greyscale Every Other Frame & 5 & 2-Layer Net & 62.47\% \\
Stacked Greyscale Every Other Frame & 5 & 3-Layer Net & 54.14\% \\
\hline
\end{tabular}
\end{center}
\caption{Results with Prior Class Distribution Normalized to Uniform}
\end{table}
Sanity check loss starts at -ln(1/3) = 1.0986
Accuracy starts at 33%

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/5_len_all_data_2_layer/plots/lr_2015-03-15-35.png}
\end{center}
   \caption{Learning Rate Inverse Decay}
\label{fig:long}
\label{fig:onecol}
\end{figure}

\begin{figure*}
<<<<<<< HEAD
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/accuracy_2015-03-15-00.png}
\\
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/loss_2015-03-15-00.png}
\end{center}
\caption{2D Optical Flow + Greyscale - Linear Classifier - Softmax}

\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/accuracy_2015-03-15-00.png}
\\
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/loss_2015-03-15-00.png}
\end{center}
\caption{2D Optical Flow + Greyscale - Linear Classifier - Softmax}

\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/accuracy_2015-03-15-00.png}
\\
\includegraphics[width=0.9\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/loss_2015-03-15-00.png}
\end{center}
\caption{2D Optical Flow + Greyscale - Linear Classifier - Softmax}

\label{fig:short}
\end{figure*}

\subsection{More Data}

normalization of class probabilities
stcking and intervals

tuning hyper params
overfitting
adding dropout

smoothing joystick data 
=======
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/1_len_3_layer/plots/accuracy_2015-03-15-55.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/1_len_3_layer/plots/loss_2015-03-15-55.png}
	\caption{Single Greyscale - 3-Layer Net}
	\label{fig:p_resA}
\end{subfigure}
\hfill
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/5_len_all_data_2_layer/plots/accuracy_2015-03-15-35.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/5_len_all_data_2_layer/plots/loss_2015-03-15-35.png}
	\caption{Stacked Greyscale - 2-Layer Net }
	\label{fig:p_resB}
\end{subfigure}
\hfill
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/10_len_2_int_norm_small_dropout/plots/accuracy_2015-03-15-48.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/10_len_2_int_norm_small_dropout/plots/loss_2015-03-15-48.png}
	\caption{Stacked Greyscale Every Other Frame - 2-Layer Net}
	\label{fig:p_resC}
\end{subfigure}
\hfill
\begin{subfigure}{\linewidth}
	\centering
	\includegraphics[width=0.38\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/accuracy_2015-03-15-57.png}
	\includegraphics[width=0.38\linewidth]{../../../../learning/10_len_2_int_norm_3_layer_dropout/plots/loss_2015-03-15-57.png}
	\caption{Stacked Greyscale Every Other Frame - 3-Layer Net}
	\label{fig:p_resD}
\end{subfigure}
\caption{Results After Normalization of Class Distribution}
\end{figure*}


\subsection{Best Performance}
The model that gave the best performance was a 3-layer convnet,conv-slkdf, trained on salkjfas images. This gave us an accuracy of salkjas.
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099


\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{pics/video.png}
\end{center}
<<<<<<< HEAD
   \caption{Labeled Input Stream. The green dot represents the correct label and the red dot represents the predicted label. The position of the dots on the input images represent the direction of the joystick data. A dot on the left edge, center, and right edge signify a left turn, straight, and right turn respectively. Link to video \href{}{here}.}
\label{fig:long}
\label{fig:onecol}
\end{figure}

=======
   \caption{Labeled Input Stream. The green dot represents a correct prediction and the yellow and red dots represent the ground truth and the predicted label respectively in a mislabeled instance. The position of the dots on the input images represent the direction of the joystick data. A dot on the left edge, center, and right edge signify a left turn, straight, and right turn respectively. Link to video \href{}{here}.}
\label{fig:vid}
\label{fig:onecol}
\end{figure}

To better understand when there is the discrepancy between the predicted and ground truth labels, we plotted the value of the predicted and ground truth labels over time \ref{fig:labelsvtime}. Predicted labels seem to be well correlated with the correct ground truth labels with some slightly offset-ted. As you can see, the predicted labels jump around less than the correct labels, indicating this may be desirable behavior that would be low in accuracy.
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/5_len_all_data_2_layer/test_videos_2015-03-15-18/video03/labels.png}
\end{center}
   \caption{Plot of Predicted Labels vs. Ground Truth Labels Over Time to check if predictions make sense. Predicted Labels seem to be well correlated with the correct ground truth labels with some slightly offsetted. As you can see, the Predicted labels jump around less than the correct labels, indicating this may be desireable behavior that would be low in accuracy}
\label{fig:long}
\label{fig:onecol}
\end{figure}

<<<<<<< HEAD
=======
We also wanted to visualize the physical distance between the predicted and correct trajectories Jackrabbot would take \ref{fig:traj}, so we plotted the top view. These trajectories are approximate since our labels are separated into 3 classes. The Jackrabbot was moving at a constant velocity of 0.5 m/s forward in our dataset and the maximum angular velocity was set at 1.571 rad/s. Each label was taken at 10 frames/s. From this, the average angle of rotation was found from the average joystick positive and negative values, $\pm0.2$, and used to plot the vector at each timestep.
\begin{center} $(0.2*1.571)*180/\pi = 18 \deg/s = 1.8 \deg/frame$
\end{center}
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../learning/5_len_all_data_2_layer/test_videos_2015-03-15-18/video03/traj.png}
\end{center}
   \caption{Top View of the Predicted and Ground Truth Labels to qualitatively measure the difference in incorrect predictions.}
\label{fig:long}
\label{fig:onecol}
\end{figure}


\section{Conclusion}
<<<<<<< HEAD
problems with joystick data, use accelerometer data
smooth data
\subsection{Future Work and Improvements}




\begin{figure}[t]
\begin{center}
\includegraphics[width=0.9\linewidth]{../../../../data_raw/plots/histLR-RStick.png}
\end{center}
   \caption{ }
\label{fig:long}
\label{fig:onecol}
\end{figure}
=======
The results from the 2-Layer net with 5 consecutive greyscale frames stacked as input achieved the highest accuracy of 65.12\%. This is 32\% better than random guessing. Despite having an accuracy of only 65.12\%, from the video stream, we think we are close to being able to deploy and test on the Jackrabbot itself. Even though, we called our human-controlled joystick data as "ground truth", there are many paths Jackrabbot could take that would be nearly identical to "ground truth" and produce the desired behavior. Futhermore, our human-controlled driving is not optimal and varies from driver to driver, so our metric of matching the ground truth labels exactly is indicative but not precise in measuring the optimal behavior.

Stacking the frames and normalizing the class distributions were the most effective, each improving our accuracy by approximately 10\%.

We learned that good visualizations are key to making valuable and sensible adjustments to the network. Without them, results can be very misleading and efforts to tweak to hyperparameters fruitless. Quantitative measures, such as accuracy, can also be misleading since skewed datasets to a specific class will give high accuracy for models that do nothing, such as the always-go-straight model. That is why having baselines to reference and having qualitative measures to gauge whether the predictions make sense is really important.

\subsection{Future Work and Improvements}
Our next steps include deploying this model onto Jackrabbot and characterizing its performance in real time. We would also like to further improve our model by means of more preprocessing of the label data and trying Recurrent Neural Networks for videos.

Most the joystick data is close to 0 \ref{fig:joy} since our turns are mostly small turns, which depending on the driver or the sense of urgency, is controlled via small adjustments to the joystick or quick bursts to the edges of the joystick axis. This variability in driver-to-driver data is also apparent in the bottom plot where the second half of the sequences has a smaller amplitude than the first half. We also do not know the exact mapping of joystick control data to the angular velocity of the Jackrabbot. For this reason, we think it'd be a good idea to smooth out the joystick data either with a simple moving average or a Kalman filter or use accelerometer data as the labels. This way, the labels would be easier to map and bin to several ($>$ 3) direction vectors. They also wouldn't jump around so much over time, and two very similar images won't produce vastly different labels, making it easier to learn. 

Something about RNNs and why we didnt try them this time and why we will and stacking optical flow \cite{RNN}
>>>>>>> 38b2084ebc4263ea71917c896b9cf5340f227099

note -- This project was used for both cs231n and cs231a. 
trying rnn's 

{\small
\bibliographystyle{ieee}

\begin{thebibliography}{1}

\bibitem{alvinn}
Pomerleau, Dean A., "ALVINN, an autonomous land vehicle in a neural network", Carnegie Mellon University, 1989. http://repository.cmu.edu/cgi/viewcontent.cgi?article=2874\&context=compsci.

\bibitem{RNN}
Donahue, Jeff and Hendricks, Lisa and Guadarrama, Sergio and Rohrbach, Marcus and Venugopalan, Subhashini and Saenko, Kate and Darrell, Trevor. \emph{Long-term Recurrent Convolutional Networks for Visual Recognition and Description}. CoRR, vol. abs/1411.4389, 2014. http://arxiv.org/abs/1411.4389.

\bibitem{caffe}
Jia, Yangqing and Shelhamer, Evan and Donahue, Jeff and Karayev, Sergey and Long, Jonathan and Girshick, Ross and Guadarrama, Sergio and Darrell, Trevor. \emph{Caffe: Convolutional Architecture for Fast Feature Embedding}. arXiv preprint arXiv:1408.5093, 2014.

% CS231N Notes
\bibitem{andrej}
Li, Fei-Fei and Karpathy, Andrej. "CS231n: Convolutional Neural Networks for Visual Recognition", 2015. http://cs231n.github.io.

% CS231A Textbooks
\bibitem{A1}
D. A. Forsyth and J. Ponce. \emph{Computer Vision: A Modern Approach (2nd Edition)}. Prentice Hall, 2011.
\bibitem{A2}
R. Hartley and A. Zisserman. \emph{Multiple View Geometry in Computer Vision}. Cambridge University Press, 2003. http://searchworks.stanford.edu/view/5628700.
\bibitem{A3}
R. Szeliski. \emph{Computer Vision: Algorithms and Applications}. Springer, 2011. http://searchworks.stanford.edu/view/9115177.
\bibitem{A4}
D. Hoiem and S. Savarese. "Representations and Techniques for 3D Object Recognition and Scene Interpretation", \emph{Synthesis lecture on Artificial Intelligence and Machine Learning}. Morgan Claypool Publishers, 2011. http://searchworks.stanford.edu/view/9379642.
\bibitem{A5}
Gary Bradski, Adrian Kaehler. \emph{Learning OpenCV}, O'Reilly Media, 2008. http://searchworks.stanford.edu/view/7734261. 
\end{thebibliography}

}

\end{document}
