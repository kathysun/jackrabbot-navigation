# Caffe files for different nets for learning. 
Includes solver.prototxt and train_val.prototxt and deploy.prototxt

# Good nets
# Format N_len_M: N frames back spaced M frames apart, giving N/M stacked iput images
10_len_2_int_norm_small_dropout
10_len_2_int_norm_3_layer_dropout
5_len_all_data_2_layer
1_len_3_layers

