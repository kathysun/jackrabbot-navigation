DEPTH=$1
INT=$2
PRETRAINED=$3
LEARN_ROOT=..
timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
video_dir=test_videos_$timestamp
#TOPIC=right_image_rect
TOPIC=$4

TEST1=$LEARN_ROOT/../test_raw/2015-03-08-16-49-43_original/$TOPIC/out.txt
TEST2=$LEARN_ROOT/../test_raw/2015-03-14-16-33-25_original/$TOPIC/out.txt
TEST3=$LEARN_ROOT/../test_raw/2015-03-14-17-35-35_original/$TOPIC/out.txt
TEST4=$LEARN_ROOT/../test_raw/2015-03-14-18-14-20_original/$TOPIC/out.txt

mkdir $video_dir
mkdir $video_dir/video01
mkdir $video_dir/video02
mkdir $video_dir/video03
mkdir $video_dir/video04

python $LEARN_ROOT/../visualization/label_images.py --color --root $LEARN_ROOT/../test_raw --out $video_dir/video01 --model_def deploy.prototxt --pretrained_model $PRETRAINED --seq $DEPTH --int $INT --mean_file data_lmdb/mean.binaryproto $TEST1 
avconv -framerate 8 -i $video_dir/video01/frame%04d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p $video_dir/video01/video.mp4

python $LEARN_ROOT/../visualization/label_images.py --color --root $LEARN_ROOT/../test_raw --out $video_dir/video02 --model_def deploy.prototxt --pretrained_model $PRETRAINED --seq $DEPTH --int $INT --mean_file data_lmdb/mean.binaryproto $TEST2 
avconv -framerate 8 -i $video_dir/video02/frame%04d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p $video_dir/video02/video.mp4

python $LEARN_ROOT/../visualization/label_images.py --color --root $LEARN_ROOT/../test_raw --out $video_dir/video03 --model_def deploy.prototxt --pretrained_model $PRETRAINED --seq $DEPTH --int $INT --mean_file data_lmdb/mean.binaryproto $TEST3
avconv -framerate 8 -i $video_dir/video03/frame%04d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p $video_dir/video03/video.mp4

python $LEARN_ROOT/../visualization/label_images.py --color --root $LEARN_ROOT/../test_raw --out $video_dir/video04 --model_def deploy.prototxt --pretrained_model $PRETRAINED --seq $DEPTH --int $INT --mean_file data_lmdb/mean.binaryproto $TEST4
avconv -framerate 8 -i $video_dir/video04/frame%04d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p $video_dir/video04/video.mp4

rm -rf $video_dir/video01/*.jpg
rm -rf $video_dir/video02/*.jpg
rm -rf $video_dir/video03/*.jpg
rm -rf $video_dir/video04/*.jpg
