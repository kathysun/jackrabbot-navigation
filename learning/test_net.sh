#!/usr/bin/env sh

TOPIC=$2
LEARN_ROOT=..
TEST3=$LEARN_ROOT/../test_raw/2015-03-14-17-35-35_original/$TOPIC/out.txt
#SIZE='272,512'
SIZE='36450,1'
ROOT=$LEARN_ROOT/../test_raw

python $LEARN_ROOT/tools/classify.py $TEST3 labels.npy --root $ROOT --model_def deploy.prototxt --pretrained_model $1 --images_dim $SIZE --mean_file data_lmdb/mean.binaryproto 2>&1 | tee caffe-test.log

#python $LEARN_ROOT/../visualization/plot_trajectory.py labels.npy ground.npy plots/path.png
