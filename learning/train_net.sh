#!/usr/bin/env sh

~/caffe/build/tools/caffe train --solver=solver.prototxt 2>&1 | tee caffe-train.log

../../visualization/plot_log.sh
