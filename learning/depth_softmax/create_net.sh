#!/usr/bin/env sh
# Create the imagenet lmdb inputs
# N.B. set the path to the imagenet train + val + test data dirs

DEST=./data_lmdb
TOOLS=~/caffe/build/tools
DATA=./
DATA_ROOT=../../data_raw/

mkdir $DEST

echo "Creating train lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --shuffle \
    --gray \
    $DATA_ROOT \
    $DATA/train.txt \
    $DEST/train_lmdb

echo "Creating val lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --shuffle \
    --gray \
    $DATA_ROOT \
    $DATA/val.txt \
    $DEST/val_lmdb

echo "Creating test lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
--shuffle \
--gray \
$DATA_ROOT \
$DATA/test.txt \
$DEST/test_lmdb

echo "Creating mean file"

$TOOLS/compute_image_mean $DATA_ROOT/train_lmdb \
$DEST/mean.binaryproto

echo "Done."
