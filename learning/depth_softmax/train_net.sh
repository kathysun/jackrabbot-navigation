#!/usr/bin/env sh

~/caffe/build/tools/caffe train --solver=learning/depth_softmax/solver.prototxt 2>&1 | tee caffe-train.log
