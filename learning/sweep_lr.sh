#!/usr/bin/env sh

../train_net.sh
../../visualization/plot_log.sh

sed -i 's/base_lr: 0.01/base_lr:0.001/g' ./solver.prototxt
../train_net.sh
../../visualization/plot_log.sh

sed -i 's/base_lr: 0.001/base_lr:0.0001/g' ./solver.prototxt
../train_net.sh
../../visualization/plot_log.sh

sed -i 's/base_lr: 0.0001/base_lr:0.00001/g' ./solver.prototxt
../train_net.sh
../../visualization/plot_log.sh

sed -i 's/base_lr: 0.00001/base_lr:0.000001/g' ./solver.prototxt
../train_net.sh
../../visualization/plot_log.sh

sed -i 's/base_lr: 0.000001/base_lr:0.01/g' ./solver.prototxt
