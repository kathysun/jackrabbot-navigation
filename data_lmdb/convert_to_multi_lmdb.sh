#!/usr/bin/env sh
# Create the imagenet lmdb inputs
# N.B. set the path to the imagenet train + val data dirs

DEST=./
TOOLS=../../learning/tools
DATA=./
DATA_ROOT=../../data_raw/
DEPTH=$1

rm -r $DEST/train_lmdb
rm -r $DEST/val_lmdb
rm $DEST/mean.binaryproto

echo "Creating train lmdb..."

$TOOLS/convert_multiimageset \
    --shuffle \
    --gray \
    --depth $DEPTH \
    $DATA_ROOT \
    $DATA/train.txt \
    $DEST/train_lmdb

echo "Creating val lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_multiimageset \
    --shuffle \
    --gray \
    --depth $DEPTH \
    $DATA_ROOT \
    $DATA/val.txt \
    $DEST/val_lmdb

echo "Creating mean file"

$TOOLS/compute_image_mean $DEST/train_lmdb \
    $DEST/mean.binaryproto

echo "Done."
