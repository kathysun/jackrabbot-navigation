#!/bin/bash

# Run from data_raw directory
# extract depth
#for f in 2015*/
#do
#	python ../preprocessing/extract_depth.py $f
#done

# extract optical flow and hog
for f in 2015*/left_image_rect/
do
	python ../preprocessing/extract_optflow.py $f
	python ../preprocessing/extract_hog.py $f
done
