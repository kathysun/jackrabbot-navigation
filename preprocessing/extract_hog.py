import cv2
import argparse
from os import listdir, makedirs
from os.path import isdir, isfile, join, basename
import glob
import numpy as np
import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure
import scipy.ndimage

parser = argparse.ArgumentParser(description='Convert images to hog features.')
parser.add_argument('root_folder', metavar='images', type=str, nargs='+',
                    help='root folder of images')
parser.add_argument('--dest', dest='out', default='', help='output path')

args = parser.parse_args()

root_folder = args.root_folder[0]

if not args.out:
  out_folder = join(root_folder, 'hog/')
else:
  out_folder = args.out
if not isdir(out_folder):
  makedirs(out_folder)

image_files = glob.glob(join(root_folder, '*.jpg'))

orient = 9
pix = 16
cells = 3

x, y = [272, 512]

print 'Extracting hog features with %i orientations and %i pixels per cell and %i cells' % (orient, pix, cells)

for image_path in image_files:
  im = cv2.imread(image_path, cv2.CV_LOAD_IMAGE_GRAYSCALE)
  
  fd, hog_image = hog(im, orientations=orient, pixels_per_cell=(pix, pix),
                      cells_per_block=(cells, cells), visualise=True)

  n_blocksx = int(np.floor(x // pix))  # number of cells in x
  n_blocksy = int(np.floor(y // pix))  # number of cells in y
  
  fd = cv2.normalize(fd,None,0,255,cv2.NORM_MINMAX)

  hog_path = join(out_folder, basename(image_path))
  cv2.imwrite(hog_path, fd)

#  fd = fd.reshape((n_blocksx, n_blocksy, orient))
#  fd = scipy.ndimage.zoom(fd, pix, order=0)
#  for i in range(orient):
#    if not isdir(join(out_folder, str(i))):
#      makedirs(join(out_folder, str(i)))
#    cv2.imwrite(join(out_folder, str(i), basename(image_path)), fd[:,:,i])
#    label_file = open(join(out_folder, str(i), 'out.txt'), 'w')
#    for line in open(join(root_folder, 'out.txt'), 'r'):
#      filename, label = line.split()
#      label_file.write("{0} {1}\n".format(join(out_folder, str(i), basename(filename)), label))
#    label_file.close()

label_file = open(join(out_folder, 'out.txt'), 'w')
with open(join(root_folder, 'out.txt'), 'r') as f:
  for line in f:
      filename, label = line.split()
      label_file.write("{0} {1}\n".format(join(out_folder, basename(filename)), label))
label_file.close()

#print fd
fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 8))

ax1.axis('off')
ax1.imshow(im, cmap=plt.cm.gray)
ax1.set_title('Input image')

# Rescale histogram for better display
hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))

ax2.axis('off')
ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
ax2.set_title('Histogram of Oriented Gradients')
if not isdir(join(out_folder, 'plots')):
    makedirs(join(out_folder, 'plots'))
plt.savefig(join(out_folder, 'plots/hog.png'))

