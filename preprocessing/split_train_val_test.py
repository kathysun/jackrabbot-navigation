import argparse
from os.path import splitext, basename, join, exists, isdir
from os import makedirs
from copy import copy
import glob
from random import shuffle, random
from collections import deque

parser = argparse.ArgumentParser(description='Generate train, val, test sets.')
parser.add_argument('--root', type=str, default='', help='root folder of images')
parser.add_argument('--dest', dest='out', default='', help='output path')
parser.add_argument('--topic', dest='topic', default='right_image_rect', help='topic for training and testing')
parser.add_argument('--seq', dest='seq_len', default=1, type=int, help='length of image sequence')
parser.add_argument('--val', dest='interval', default=1, type=int, help='interval between images in stack')
parser.add_argument('--mirror', dest='mirror', action='store_true', help='set this flag to include mirrored images')
parser.add_argument('--no_norm', dest='no_norm', action='store_true', help='set this flag if you dont want to normalize distribution')

args = parser.parse_args()

root_folder = args.root
if not args.out:
  out_folder = root_folder
else:
  out_folder = args.out

topic_dir = args.topic

image_files = glob.glob(join(root_folder, '*.jpeg'))
image_files.sort()
if not isdir(out_folder):
  makedirs(out_folder)

if args.mirror:
    label_files = glob.glob(join(root_folder, '*/{0}/out.txt'.format(topic_dir)))
else:
    label_files = glob.glob(join(root_folder, '*_original/{0}/out.txt'.format(topic_dir)))
N = len(label_files)
perm = range(N)
shuffle(perm)

train_file = open(join(out_folder, 'train.txt'), 'w')
val_file = open(join(out_folder, 'val.txt'), 'w')

sequence = []
interval = args.interval

print "Splitting {0} observations...".format(N)
i = 0

training_dist = {}
lines = []
for i in range(int(round(0.85*N))):
  sequence = []
  for line in open(label_files[perm[i]], 'r'):
    filename, label = line.split()
    label = int(label)
    sequence.insert(0, filename)
    if len(sequence) < args.seq_len:
        continue
    if len(sequence) > args.seq_len:
        sequence.pop()
    if label not in training_dist:
        training_dist[label] = 0
    training_dist[label] += 1
    line_out = "{0} {1}\n".format(" ".join(sequence[::interval]), label)
    lines.append((line_out, label))

p_keep = {}
min_count = min(training_dist.values())
for cls, count in training_dist.iteritems():
    p_keep[cls] =  float(min_count) / float(count)

final_dist = {}
for line in lines:
    if line[1] not in final_dist:
        final_dist[line[1]] = 0
    if args.no_norm or random() < p_keep[line[1]]:
        train_file.write(line[0])
        final_dist[line[1]] += 1
print final_dist
print "Generated training set 85% = {0}".format(int(round(0.7*N)))

lines = []
val_dist = {}
for i in range(int(round(0.85*N)), N):
  sequence = []
  for line in open(label_files[perm[i]], 'r'):
    filename, label = line.split()
    label = int(label)
    sequence.insert(0, filename)
    if len(sequence) < args.seq_len:
        continue
    if len(sequence) > args.seq_len:
        sequence.pop()
    if label not in val_dist:
        val_dist[label] = 0
    val_dist[label] += 1
    line_out = "{0} {1}\n".format(" ".join(sequence[::interval]), label)
    lines.append((line_out, label))
print "Generated validation set 15% = {0}".format(int(round(0.9*N))-int(round(0.7*N)))

p_keep = {}
min_count = min(val_dist.values())
for cls, count in val_dist.iteritems():
    p_keep[cls] =  float(min_count) / float(count)

final_dist = {}
for line in lines:
    if line[1] not in final_dist:
        final_dist[line[1]] = 0
    if args.no_norm or random() < p_keep[line[1]]:
        val_file.write(line[0])
        final_dist[line[1]] += 1
print final_dist


train_file.close()
val_file.close()
