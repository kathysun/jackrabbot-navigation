import numpy as np
import matplotlib
matplotlib.use('agg')
import skimage.io
import argparse
from os import listdir, makedirs
from os.path import isdir, join, exists, basename

parser = argparse.ArgumentParser(description='Augment the data')
parser.add_argument('--root', dest='root', default='', help='data root directory')
args = parser.parse_args()


def write_mirror_image(image, label):
    mirrored = np.fliplr(image)
    new_label = label
    if label == 1:
        new_label = 2
    elif label == 2:
        new_label = 1
    return (mirrored, new_label)


# augmentation functions must take an image and label and return the augmented image and label
aug_to_f = {'mirror':write_mirror_image}

root_dirs = [d for d in listdir(args.root) if isdir(join(args.root, d))]
for base_directory in root_dirs:
    print "Augmenting {0}".format(base_directory)
    directory = join(args.root, base_directory)
    for aug, func in aug_to_f.iteritems():
        new_base = "{0}_{1}".format(base_directory, aug)
        new_dir = join(args.root, new_base)
        if not exists(new_dir):
            makedirs(new_dir)
        else:
            print "{0} augmentation for {1} already done".format(aug, base_directory)
            continue
        subdirs = [d for d in listdir(directory) if isdir(join(directory, d))]
        for subdir in subdirs:
            makedirs(join(new_dir, subdir))
            out_labels = open(join(new_dir, subdir, "out.txt"), 'w')
            if not exists(join(directory, subdir, "out.txt")):
                continue
            in_labels = open(join(directory, subdir, "out.txt"), 'r')
            for line in in_labels:
                image_file, label = line.split()
                label = int(label)
                new_image, new_label = func(skimage.io.imread(join(args.root, image_file)), label)
                new_image_file = join(new_base, subdir, basename(image_file))
                skimage.io.imsave(join(args.root, new_image_file), new_image)
                out_labels.write("{0} {1}\n".format(new_image_file, new_label))

            out_labels.close()
            in_labels.close()



