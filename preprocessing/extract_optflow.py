import cv2
import argparse
from os import listdir, makedirs
from os.path import isdir, isfile, join, basename
import glob
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Convert images to optical flow.')
parser.add_argument('root_folder', metavar='images', type=str, nargs='+',
                    help='root folder of images')
parser.add_argument('--dest', dest='out', default='', help='output path')

args = parser.parse_args()

root_folder = args.root_folder[0]
if not args.out:
  out_folder = join(root_folder, 'optflow/')
else:
  out_folder = args.out

image_files = glob.glob(join(root_folder, '*.jpg'))

image_files.sort()
if not isdir(out_folder):
  makedirs(out_folder)

optx = []
opty = []
dir = []

print 'Extracting optical flow'

prev = cv2.imread(image_files[0], cv2.CV_LOAD_IMAGE_GRAYSCALE)
hsv = np.zeros((prev.shape[0], prev.shape[1], 3), np.uint8)
hsv[...,1] = 255
for image_path in image_files[1:]:
  out_path = join(out_folder, basename(image_path))
  
  next = cv2.imread(image_path, cv2.CV_LOAD_IMAGE_GRAYSCALE)
  flow = cv2.calcOpticalFlowFarneback(prev, next, 0.5, 3, 15, 3, 5, 1.2, 0)
  prev = next

  optx.append(np.mean(flow[:,:,0]))
  opty.append(np.mean(flow[:,:,1]))
  
  mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
  hsv[...,0] = ang*180/np.pi/2
  hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
  rgb = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
    
#cv2.imshow('flow',rgb)
  flow = cv2.normalize(flow,flow,0,255,cv2.NORM_MINMAX)
  flow = np.dstack((flow, next))
  cv2.imwrite(out_path, flow)

label_file = open(join(out_folder, 'out.txt'), 'w')
with open(join(root_folder, 'out.txt'), 'r') as f:
  first_line = f.readline()
  for line in f:
      filename, label = line.split()
      dir.append(-(((int(label)+1)%3)-1))
      label_file.write("{0} {1}\n".format(join(out_folder, basename(filename)), label))
label_file.close()

n = range(len(dir))
plt.figure()
plt.plot(n, optx, 'b', n, opty, 'r', n, dir, 'g')
plt.legend(['average x flow', 'average y flow', 'joy labels'])
plt.title('Average flow of image compared to joystick labels')
if not isdir(join(out_folder, 'plots')):
    makedirs(join(out_folder, 'plots'))
plt.savefig(join(out_folder, 'plots/opt.png'))
