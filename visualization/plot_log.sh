#!/usr/bin/env sh

mkdir plots
timestamp=$(date +"%Y-%m-%d-%H-%M-%S")

# Plot accuracy
python ../../visualization/plot_training_log.py 0 "plots/accuracy_$timestamp.png" caffe-train.log 
# Plot loss
python ../../visualization/plot_training_log.py 2 "plots/loss_$timestamp.png" caffe-train.log 
# Plot learning rate
python ../../visualization/plot_training_log.py 10 "plots/lr_$timestamp.png" caffe-train.log 
