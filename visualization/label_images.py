import argparse
import matplotlib
matplotlib.use('agg')
import skimage.io
import skimage.draw
import glob
import caffe
import numpy as np
from collections import deque
from os.path import isdir, exists, join
import matplotlib.pyplot as plt

NO_CLASS = -1

parser = argparse.ArgumentParser(description='Label images')
parser.add_argument('labels', metavar='label_file', type=str, nargs=1, help='label file for images')
parser.add_argument('--root', dest='root', default='', help='root directory')
parser.add_argument('--out', dest='out_path', default='', help='output path')
parser.add_argument('--model_def', help='path to model definition file')
parser.add_argument('--pretrained_model', help='path to pretrained model')
parser.add_argument('--seq', dest='seq', default=1, type=int, help='Sequence depth')
parser.add_argument('--int', dest='interval', default=1, type=int, help='Sequence interval')
parser.add_argument("--mean_file", default="", help="mean image file")
parser.add_argument("--color", action='store_true', help="color images if true")
parser.add_argument("--pred", help='file for predicted npy')
        

args = parser.parse_args()

# Right is negative, Left is positive
def map_labels_to_dir(labels):
    return ((np.array(labels)+1)%3-1)

def plot_labels(predictions, ground, dest):
    n = range(len(predictions))
    ax = plt.axes()
    plt.plot(n, map_labels_to_dir(predictions), 'b', n, map_labels_to_dir(ground), 'g')
    plt.legend(['Predicted', 'Ground Truth'])
    plt.title('Labels Over Frame')
    plt.savefig(join(dest, 'labels.png'))

def labels_for_images(images_files, seq, interval, model_def, pretrained, mean, color):
    channel_swap = None;
#    if color:
#        channel_swap = np.array([2,1,0])
#        channel_swap = np.tile(channel_swap, seq)
    
    classifier = caffe.Classifier(model_def, pretrained, mean=mean, raw_scale=255.0, channel_swap=channel_swap)
    sequence = []
    predictions = []

    i = 0
    channels = 1
    for image_file in image_files:
        if i % 50 == 0:
	    print "Image {0} of {1}".format(i, len(image_files))
        image = caffe.io.load_image(image_file, color)
        if image.ndim == 3:
            channels = image.shape[2]
            [sequence.insert(0, np.reshape(image[:,:,i], (image.shape[0], image.shape[1], 1))) for i in range(channels)]
        else:
            image = np.reshape(image, (image.shape[0], image.shape[1], 1))
            sequence.insert(0, image)
        
        if len(sequence) < seq*channels:
            predictions.append(NO_CLASS)
            continue
        if len(sequence) > seq*channels:
            [sequence.pop() for i in range(channels)]

        stacked = np.concatenate(sequence[::interval], axis=2)
        probs = classifier.predict([stacked], False)
        prediction = np.argmax(probs)
        predictions.append(prediction)
        i += 1

    return predictions

def compute_stats(correct_labels, predicted_labels, dest):
    predicted = np.array(predicted_labels)
    correct = np.array(correct_labels)
    correct_to_counter = {}
    all_classes = set(correct_labels).union(predicted_labels)
    num_classes = len(all_classes)
    if NO_CLASS in all_classes:
        num_classes -= 1
    conf = np.zeros((num_classes, num_classes))
    for cl, pl in zip(correct_labels, predicted_labels):
        if cl is NO_CLASS or pl is NO_CLASS:
            continue
        conf[cl, pl] += 1

    np.save(join(dest, 'confusion.npy'), conf)

    fig = plt.figure()
    plt.clf()
    ax = fig.add_subplot(111)
    ax.matshow(conf)

    for x in range(num_classes):
        for y in xrange(num_classes):
            ax.annotate(str(conf[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center')
    plt.title('Confusion Matrix')
    plt.savefig(join(dest, 'confusion.png'))
    
    with open(join(dest, 'stats.txt'), 'w') as stats_file:
        stats_file.write("Accuracy: {0}\n".format(np.mean(correct == predicted)))


def produce_labeled_images(image_files, out, correct_labels, predicted_labels=None):
    count = 0
    class_to_circle = None
    if predicted_labels is None:
        predicted_labels = [NO_CLASS] * len(image_files)
    for image_file, correct, predicted in zip(image_files, correct_labels, predicted_labels):
        image = skimage.io.imread(image_file)
        rows = 0
        cols = 0
        c = 1
        if image.ndim == 2:
            rows, cols = image.shape
            image = np.reshape(image, (rows, cols, 1))
        else:
            rows, cols, c = image.shape

        if c == 1:
            image = np.concatenate((image, image, image), axis=2)
            c = 3

        if class_to_circle is None:
            class_to_circle = {}
            class_to_circle[0] = skimage.draw.circle(rows - 15, cols/2, 8)
            class_to_circle[2] = skimage.draw.circle(rows - 15, 15, 8)
            class_to_circle[1] = skimage.draw.circle(rows - 15, cols-15, 8)
 
        predicted_color = (0, 255, 0) if correct == predicted else (255, 0, 0)
        if correct == predicted and correct is not NO_CLASS:
            image[class_to_circle[correct]] = np.array([0, 255, 0])
        else:
            if correct is not NO_CLASS:
                image[class_to_circle[correct]] = np.array([255, 255, 0])
            if predicted is not NO_CLASS:
                image[class_to_circle[predicted]] = np.array([255, 0, 0])
        out_file = join(out, "frame{:04}.jpg".format(count))
        skimage.io.imsave(out_file, image)
        count += 1

image_files = []
correct_labels = []
mean = None

if args.mean_file:
    if args.mean_file.endswith('binaryproto'):
        blob = caffe.proto.caffe_pb2.BlobProto()
        data = open( args.mean_file , 'rb' ).read()
        blob.ParseFromString(data)
        arr = np.array( caffe.io.blobproto_to_array(blob) )
        mean = arr[0]
        np.save( args.mean_file.split('.')[0]+'npy' , mean )
    else:
        mean = np.load(args.mean_file)

with open(args.labels[0], 'r') as label_file:
    for line in label_file:
        file_name, label = line.split()
        label = int(label)
        image_files.append(join(args.root, file_name))
        correct_labels.append(label)

print "Classifying images"
predicted_labels = None
if args.pred is not None and exists(args.pred):
    predicted_labels = np.load(args.pred)
elif args.model_def is not None and args.pretrained_model is not None:
    predicted_labels = labels_for_images(
        image_files,
        args.seq,
        args.interval,
        args.model_def,
        args.pretrained_model,
        mean,
        args.color
    )
    np.save(join(args.out_path, 'predicted.npy'), predicted_labels)

print "Computing stats"
compute_stats(correct_labels, predicted_labels, args.out_path)

print "Plotting labels over time"
plot_labels(predicted_labels[1:], correct_labels[1:], args.out_path)

print "Producing video"
produce_labeled_images(image_files, args.out_path, correct_labels, predicted_labels)

