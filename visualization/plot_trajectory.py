#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import math
import random

def map_label(label):
  angle = 1.8
  if label == 0:
    return 0
  elif label == 1:
    return angle
  elif label == 2:
    return -angle
  else:
    return 0
    print 'invalid label ' + str(label)

# 0 is straight, 1 is left, 2 is right
def draw_path(predicted, ground, dest):
  speed = 0.5
  start_x, start_y = [0, 0]

  pred_ang = [map_label(p) for p in predicted]
  gt_ang = [map_label(p) for p in ground]
  
  pred_cum = np.add.accumulate(pred_ang)
  gt_cum = np.add.accumulate(gt_ang)
  
  pred_x = np.add.accumulate(speed * np.cos(np.radians(pred_cum + 90)))
  pred_y = np.add.accumulate(speed * np.sin(np.radians(pred_cum + 90)))
  pred_x = np.append(start_x, pred_x)
  pred_y = np.append(start_y, pred_y)
  
  gt_x = np.add.accumulate(speed * np.cos(np.radians(gt_cum + 90)))
  gt_y = np.add.accumulate(speed * np.sin(np.radians(gt_cum + 90)))
  gt_x = np.append(start_x, gt_x)
  gt_y = np.append(start_y, gt_y)
  
  ax = plt.axes()
  plt.plot(pred_x, pred_y, 'b', gt_x, gt_y, 'g')
  plt.axis('off')
  #plt.xaxis([-10,10])
  plt.legend(['Predicted', 'Ground Truth'])
  plt.title('Trajectory')
  plt.savefig(dest)
  plt.show()


if __name__ == '__main__':
  if len(sys.argv) != 4:
#    gt = [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0]
#    pred = [random.randint(0, 2) for i in range(len(gt))]
#    draw_path(pred, gt, 'path.png')
    print 'Usage: python plot_trajectory.py predicted/label.npy ground.txt destination.png'
  else:
    pred_file = sys.argv[1]
    if not pred_file.endswith('.npy'):
      print 'Path must ends with npy' % pred_file
      exit
    else:
      pred = np.load(pred_file)
    ground_file = sys.argv[2]
    if ground_file.endswith('.txt'):
      ground =[int(line.split()[1]) for line in open(ground_file, 'r')]
    else:
      ground = np.load(ground_file)
    dest = sys.argv[3]

    print np.mean(pred==ground)
#    print pred
    draw_path(pred, ground, dest)

